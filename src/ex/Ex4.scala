package ex

object Ex4 {

  // curried function
  val p1: Int => Int => Int => Boolean =
    x => y => z => (x <= y) && (y <= z)

  // non-curried function
  val p2: (Int, Int, Int) => Boolean =
    (x, y, z) => (x <= y) && (y <= z)

  // curried method
  def p3(x: Int)(y: Int)(z: Int): Boolean =
    (x <= y) && (y <= z)

  // non-curried method
  def p4(x: Int, y: Int, z: Int): Boolean =
    (x <= y) && (y <= z)

}
