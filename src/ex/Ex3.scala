package ex

object Ex3 {

  // Ex. 3.a
  val funParity: Int => String = {
    case n if n % 2 == 0 => "even"
    case _ => "odd"
  }

  // Ex. 3.a
  def metParity(x: Int): String = x match {
    case n if n % 2 == 0 => "even"
    case _ => "odd"
  }

  // Ex 3.b
  val neg: (String => Boolean) => (String => Boolean) =
    f => (x => !f(x))

  // Ex 3.c
  def genericNeg[A](f: A => Boolean): (A => Boolean) =
    s => !f(s)

}
