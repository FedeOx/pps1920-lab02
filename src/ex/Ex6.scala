package ex

object Ex6 {

  // NOTA: non è tail recursion perché la chiamata ricorsiva non è l'ultima cosa che viene fatta prima di terminare
  // la ricorsione. L'ultima cosa che viene fatta è l'operazione somma.
  def fib(n: Int): Int = n match {
    case 0 => 0
    case 1 | 2 => 1
    case x if x > 2 => fib(n-1) + fib(n-2)
  }

}
