package ex

object Ex7 {

  sealed trait Shape
  object Shape {
    case class Rectangle(side1: Double, side2: Double) extends Shape
    case class Circle(radius: Double) extends Shape
    case class Square(side: Double) extends Shape

    def perimeter(shape: Shape): Double = shape match {
      case Rectangle(s1, s2) => s1 * 2 + s2 *2
      case Square(s) => s * 4
      case Circle(r) => 2 * Math.PI * r
    }

    def area(shape: Shape): Double = shape match {
      case Rectangle(b, h) => b * h
      case Square(s) => s * s
      case Circle(r) => Math.PI * Math.pow(3, 2)
    }
  }

}
