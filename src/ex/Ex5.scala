package ex

object Ex5 {

  def compose(f: Int => Int, g: Int => Int): Int => Int =
    x => f(g(x))

  def genericCompose[A, B <: A] (f: A => B, g: A => B): A => B =
    x => f(g(x))

}
