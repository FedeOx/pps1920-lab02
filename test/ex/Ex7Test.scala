package ex

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import ex.Ex7.Shape._

class Ex7Test {

  val RECTANGLE_SIDE1 = 5;
  val RECTANGLE_SIDE2 = 10;
  val RECTANGLE_ACTUAL_PERIMETER: Double = RECTANGLE_SIDE1 * 2 + RECTANGLE_SIDE2 * 2;
  val RECTANGLE_ACTUAL_AREA: Double = RECTANGLE_SIDE1 * RECTANGLE_SIDE2;

  val SQUARE_SIDE = 5;
  val SQUARE_ACTUAL_PERIMETER: Double = SQUARE_SIDE * 4;
  val SQUARE_ACTUAL_AREA: Double = SQUARE_SIDE * SQUARE_SIDE;

  val CIRCLE_RADIUS = 3;
  val CIRCLE_ACTUAL_PERIMETER: Double = 2 * Math.PI * CIRCLE_RADIUS;
  val CIRCLE_ACTUAL_AREA: Double = Math.PI * Math.pow(CIRCLE_RADIUS, 2);

  @Test def testRectanglePerimeter(): Unit = {
    assertEquals(perimeter(Rectangle(RECTANGLE_SIDE1, RECTANGLE_SIDE2)), RECTANGLE_ACTUAL_PERIMETER)
  }

  @Test def testSquarePerimeter(): Unit = {
    assertEquals(perimeter(Square(SQUARE_SIDE)), SQUARE_ACTUAL_PERIMETER)
  }

  @Test def testCirclePerimeter(): Unit = {
    assertEquals(perimeter(Circle(CIRCLE_RADIUS)), CIRCLE_ACTUAL_PERIMETER);
  }

  @Test def testRectangleArea(): Unit = {
    assertEquals(area(Rectangle(RECTANGLE_SIDE1, RECTANGLE_SIDE2)), RECTANGLE_ACTUAL_AREA);
  }

  @Test def testSquareArea(): Unit = {
    assertEquals(area(Square(SQUARE_SIDE)), SQUARE_ACTUAL_AREA)
  }

  @Test def testCircleArea(): Unit = {
    assertEquals(area(Circle(CIRCLE_RADIUS)), CIRCLE_ACTUAL_AREA)
  }

}
