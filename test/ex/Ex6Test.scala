package ex

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import ex.Ex6._

import scala.util.control.Exception

class Ex6Test {

  @Test def testFib(): Unit = {
    assertEquals(fib(0), 0)
    assertEquals(fib(1), 1)
    assertEquals(fib(2), 1)
    assertEquals(fib(3), 2)
    assertEquals(fib(4), 3)
    assertEquals(fib(5), 5)
  }

  @Test def testFibWithNegativeNumbers(): Unit = {
    try {
      fib(-1);
      fail();
    } catch {
      case e: MatchError => println("Match Error catched")
    }

  }

}
