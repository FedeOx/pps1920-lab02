package ex

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import ex.Ex5._

class Ex5Test {

  @Test def testComposition() : Unit = {
    assertEquals(compose(_ - 1, _ * 2)(5), 9);
  }

  @Test def testCompositionExtended(): Unit = {
    assertEquals(compose(x => x - 1, y => y * 2)(5), 9);
  }

  @Test def testGenericComposition(): Unit = {
    val f1 = (x: Double) => x / 2
    val g1 = (y: Double) => y * 2
    assertEquals(genericCompose(f1, g1)(5), 5);
  }
}
