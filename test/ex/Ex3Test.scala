package ex

import org.junit.jupiter.api.Assertions._
import ex.Ex3._
import org.junit.jupiter.api.Test

class Ex3Test {

  @Test def testParity(): Unit = {
    assertEquals(funParity(6), "even")
    assertEquals(funParity(3), "odd")

    assertEquals(metParity(6), "even")
    assertEquals(metParity(3), "odd")
  }

  @Test def testParityWithNegativeNumbers(): Unit = {
    assertEquals(funParity(-1), "odd")
    assertEquals(funParity(-2), "even")

    assertEquals(metParity(-1), "odd")
    assertEquals(metParity(-2), "even")
  }

  @Test def testWithNumbers() {
    val isOne: Int => Boolean = _ == 1
    val notIsOne = genericNeg(isOne);
    assertTrue(notIsOne(0))
    assertFalse(notIsOne(1))
    assertTrue(notIsOne(0) && !notIsOne(1))
  }

  @Test def testWithStrings() {
    val empty: String => Boolean = _ == ""
    val notEmpty = genericNeg(empty)
    assertTrue(notEmpty("foo"))
    assertFalse(notEmpty(""))
    assertTrue(notEmpty("foo") && !notEmpty(""))
  }

}
