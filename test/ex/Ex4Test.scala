package ex

import org.junit.jupiter.api.Assertions._
import ex.Ex4._
import org.junit.jupiter.api.Test

class Ex4Test {

  @Test def testP1AndP2EqualSemantic() {
    assertEquals(p1(2)(3)(4), p2(2,3,4))
    assertEquals(p1(2)(4)(3), p2(2,4,3))
  }

  @Test def testP3AndP4EqualSemantic() {
    assertEquals(p3(2)(3)(4), p4(2,3,4))
    assertEquals(p3(2)(4)(3), p4(2,4,3))
  }

}
