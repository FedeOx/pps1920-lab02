package ex

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import ex.Ex8.Option._

class Ex8Test {

  @Test def testFilterWithSome(): Unit = {
    assertEquals(filter(Some(5))(_ > 2), Some(5))
    assertEquals(filter(Some(5))(_ > 8), None())
  }

  @Test def testFilterWithNone(): Unit = {
    val pred = (x: Int) => x > 8;
    assertEquals(filter(None())(pred), None())
  }

  @Test def testMapWithSome(): Unit = {
    assertEquals(map(Some(5))(_ > 2), Some(true))
  }

  @Test def testMapWithNone(): Unit = {
    val fun = (x: Int) => x > 2;
    assertEquals(map(None())(fun), None())
  }

  @Test def testMap2WithSome(): Unit = {
    assertEquals(map2(Some(5))(Some(3))(_+_), Some(8))
    assertEquals(map2(Some("Hello "))(Some("World!!"))(_+_), Some("Hello World!!"))
  }

  @Test def testMap2WithNone(): Unit = {
    assertEquals(map2(None():None[Int])(Some(3))(_+_), None())
    assertEquals(map2(Some("Hello "))(None())(_+_), None())
  }

}
